const libButton = document.querySelector("#lib-button");

function makeMadLib() {
  let story = document.querySelector("#story");
  let noun = document.querySelector("#noun").value;
  let adjective = document.querySelector("#adjective").value;
  let person = document.querySelector("#person").value;

  story.textContent = `${person} really likes ${adjective} ${noun}`;
}

libButton.addEventListener("click", makeMadLib);
