let solution = document.getElementById("solution");
const squareButton = document.getElementById("square-button");
const halfButton = document.getElementById("half-button");
const percentageButton = document.getElementById("percent-button");
const areaButton = document.getElementById("area-button");
function square(num) {
  return num ** 2;
}

function half(num) {
  return (num / 2).toFixed(2);
}

function percentageOf(num1, num2) {
  return `${((num1 / num2) * 100).toFixed(2)}%`;
}

function circleArea(radius) {
  return (Math.PI * radius ** 2).toFixed(2);
}

squareButton.addEventListener("click", function () {
  let num = document.querySelector("#square-input").value;
  solution.textContent = square(num);
});

halfButton.addEventListener("click", function () {
  let num = document.querySelector("#half-input").value;
  solution.textContent = half(num);
});

percentageButton.addEventListener("click", function () {
  let num1 = document.querySelector("#percent1-input").value;
  let num2 = document.querySelector("#percent2-input").value;
  solution.textContent = percentageOf(num1, num2);
});

areaButton.addEventListener("click", function () {
  let num = document.querySelector("#area-input").value;
  solution.textContent = circleArea(num);
});

// BONUS
let squareInput = document.querySelector("#square-input");
squareInput.addEventListener("keypress", function (e) {
  if (e.key === "Enter") {
    let num = document.querySelector("#square-input").value;
    solution.textContent = square(num);
  }
});

let halfInput = document.querySelector("#half-input");
halfInput.addEventListener("keypress", function (e) {
  if (e.key === "Enter") {
    let num = document.querySelector("#half-input").value;
    solution.textContent = half(num);
  }
});

let percInput = document.querySelector("#percent2-input");
percInput.addEventListener("keypress", function (e) {
  if (e.key === "Enter") {
    let num1 = document.querySelector("#percent1-input").value;
    let num2 = document.querySelector("#percent2-input").value;
    solution.textContent = percentageOf(num1, num2);
  }
});

let areaInput = document.querySelector("#area-input");
areaInput.addEventListener("keypress", function (e) {
  if (e.key === "Enter") {
    let num = document.querySelector("#area-input").value;
    solution.textContent = circleArea(num);
  }
});
