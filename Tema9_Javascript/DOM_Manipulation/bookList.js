const body = document.body;
let books = [
  {
    title: "The Design of EveryDay Things",
    author: "Don Norman",
    alreadyRead: false,
    imageSrc:
      "https://libris.to/media/jacket/01387893_the-design-of-everyday-things.jpg",
    bookCoverLink:
      "https://www.google.com/aclk?sa=l&ai=DChcSEwjqxYC9pNr6AhVD6HcKHYNfDNMYABABGgJlZg&ae=2&sig=AOD64_2DbZCCFHSuzhLUajBXHg9Mn48XYA&adurl&ctype=5&ved=2ahUKEwiuyfK8pNr6AhWBnv0HHZHGC8EQvhd6BAgBEFI",
  },
  {
    title: "The Most Human Human",
    author: "Brian Christian",
    alreadyRead: true,
    imageSrc:
      "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1295465264l/8884400.jpg",
    bookCoverLink:
      "https://www.goodreads.com/book/show/8884400-the-most-human-human",
  },
];

books.forEach(function (book) {
  let newParagraph = document.createElement("p");
  let newText = document.createTextNode(`${book.title} by ${book.author} `);
  newParagraph.appendChild(newText);
  body.appendChild(newParagraph);
});

// BONUSES
const newList = document.createElement("ul");
books.forEach(function (book) {
  let newItem = document.createElement("li");
  let itemText = document.createTextNode(`${book.title} by ${book.author} `);
  newItem.appendChild(itemText);

  let newLink = document.createElement("a");
  let newImg = document.createElement("img");

  newLink.href = book.bookCoverLink;
  newLink.target = "_blank";

  newImg.src = book.imageSrc;
  newImg.alt = "Book Cover Image";
  newImg.width = "100";
  newImg.height = "100";

  newLink.appendChild(newImg);
  newItem.appendChild(newLink);
  if (book.alreadyRead === true) {
    newItem.style.color = "green";
  } else {
    newItem.style.color = "red";
  }
  newList.appendChild(newItem);
});
body.appendChild(newList);
