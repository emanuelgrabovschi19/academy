const body = document.body;

// 1.
body.style.fontFamily = "Arial, sans-serif";

// 2.
let spansArr = document.querySelectorAll("span");
spansArr[0].textContent = "Emi";
spansArr[1].textContent = "Gym, Music, Travel";
spansArr[2].textContent = "Galati";

// 3.
let listItems = document.querySelectorAll("li");
listItems.forEach((item) => (item.className = "lsit-item"));

// 4.
let myImg = document.createElement("img");
myImg.src =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHwAAAB8CAMAAACcwCSMAAAAMFBMVEXBx9D///+9w83g4+fJztbz9PbQ1Nvp6+7FytP4+fr8/P3a3ePd4OXu8PLj5unM0dlzI2nQAAAFBUlEQVRogcWbWZKEIBBEERUQUe9/23HtZifLhpj8m4genyAUVQmwji49ynWexaDYLjWIeV7lqF88iBG5Sy8OJGeOzj9FvxDfgAIfV6G4h3VfQYl1bAGf+s1vb/wFtn6qDJdbrskenwtZD657BZNvvuqRz1+GT4YGfmTK+BJc9+/Qh4qtL8DlQOxwW3wofPssfBI/oE+8yI78HLz/EX3i+1fwaavA3ulbuvFJuERCCkRnyS+fgs91yJdmGvzXkeaKCwJcq5roQyo65WPwsTZ6F4+tdhF4C/auBYE3YjMWtj2AN2NH6D5ct2Mz5o86H159nNsa8nBBeBS/RfgXkYMb9EmcDcLIZRwXacQAR2Ju0nCJtkAZO0/Xo4G/lkzBJ/D1YzkCnHXYa5wNHzD2GqIPgau/PegsOPTB+ZZMzDSUANif/QufoH9NrY6nZugR347/wqFZZmJMYud959sHLpFaqMDG2s4/4/WBa2S0JXICW0j/Dc+weeDIWFVlNhSfPxntDUemeDQfCDQin31y4Eg9lh3oXyHpn7HhmvC6JUGdqC04Ug2CDcfGXG/BkXUB9jugXOgLR+a4nwhkBMzaa64zsKPK8eUrJM6JB45EdR7JfFNakOdNNxxaDGGLCRzv/Q3fgB8PBH8PCtXbBYdG54azsdYcs2eHr7R1EBAy04+EiIELeXX48UQG1sP14XvVzKCJ0QK+T14GZp3VR/sx2RhaIVHmOVZCiB2OsWtHuEMdQ2tiSmwHH6nZAv6y7qp2amFwcVh3PT8k2Qr+Ml+r2ILqlkMrw63GijncpZkAB5tOeCDDjZCKefslwdChycABT3keI9hPyJijuNU05yu7bXCKtDlBtN1SlsgjdN6+VDbKwjP8pbhIezL0HQKq35ncMoHjtIUmTI0H/5MPZ2kgBBkLr3on4Iwrdaf3lHgFv7zXdRl3LSvBe3VFie3BC9BdZx/eeGrmtOLJRH1JOI1qoAVOIBtIo6lzCx1FQ+OAnJSAy6X6OsslyEEhC3jmgpTIfBCz6UkysyjG+rNELpkY86sDX50eC7FzK9oiheMeeWUPm9y2SK68IdSmMWXCJx8LVhjFEYgr7RPcVlhyssXPGhDpieH8MQET9idWoZSUevhjfyackWKSjinu5X+M37jlDW3nIIp1vGV5xxYXDp7mKyvatO4Lj3RNjdF2KTbm7G2OcIMndajnjcJY42zwhAZSqSqjKAyhztZW6GVQfLeSwnXT3dQLAk2dSX7JNyv87cwgDraEBxu5/oRoCA+3sP0w1xAe2bz3gnA7ePTYgrsX1wweP7DhuljN4ImjKs5krznPHXjqkI6T9ahqTdf2UE4eT/I+e6W2aytL82zEzJG0Oj2vbcckeyTNXfkp29YpOXHdz0+CY4jOtEibbqCcJYOXjiG6WTxXP+Uz3lnt4gHMvaOYg5/fN94rGYCjp34F87rxfrEUmbrAceN39Zp/cAI8bhymfHR871Xo8cILPWIuCAFvMp4lSTpi3sWs801CQ08vIjBiiYfrY9UtV3PxWtZohtASIV8r2DsvYmxwJWT688s5Yj7z4cWFii510odzYby7cXqUvWBRG+jlVZIu72scpveh7TC8d2z8Rd9foulKGwg8uLLn9fgv14e6/704tWt6tx3ATTkwYZflqPsYvNxqFL5Liv+6JnjqHy9InipeDeVqa3M19NJ0X4qNqe2l2Ed6Ca8DU7mn/gCjpDDFaYkmzAAAAABJRU5ErkJggg==";
myImg.alt = "Profile picture";
body.appendChild(myImg);
