const img = document.querySelector("img");
img.style.position = "absolute";
img.style.left = "0px";
let animated = 0;

// function catWalk() {
//   let oldLeft = parseInt(img.style.left);
//   let newLeft = oldLeft + 10;
//   img.style.left = `${newLeft}px`;

//   // BONUS 1
//   if (newLeft > window.innerWidth - img.width) {
//     img.style.left = "0px";
//   }
// }

// BONUS 3
const delay = (ms) => new Promise((res) => setTimeout(res, ms));

const swapImage = async () => {
  await delay(10000);
  img.src = "http://www.anniemation.com/clip_art/images/cat-walk.gif";
};

// BONUS 2
let toRight = true;
function catWalk() {
  let leftPos = parseInt(img.style.left);
  const middle = window.innerWidth / 2;
  if (animated === 0 && leftPos >= middle) {
    animated = 1;
    img.src = "https://media2.giphy.com/media/BK1EfIsdkKZMY/giphy.gif";
    swapImage();
  }

  if (toRight && leftPos > window.innerWidth - img.width) {
    toRight = false;
    img.style.transform = "rotate(180deg)";
  }
  if (!toRight && leftPos <= 0) {
    toRight = true;
    img.style.transform = "rotate(0deg)";
  }
  if (toRight) {
    img.style.left = `${leftPos + 10}px`;
  } else {
    img.style.left = `${leftPos - 10}px`;
  }
}
window.setInterval(catWalk, 50);
