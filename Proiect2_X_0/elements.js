// Selectare elemente DOM
const btnSingle = document.getElementById("btn-single");
const btnMulti = document.getElementById("btn-multi");
const gameButtons = document.querySelectorAll(".btn-mode");
const startButtons = document.querySelectorAll(".btn-start");
const gameDetailsSingle = document.querySelector(".game-details-single");
const gameDetailsMulti = document.querySelector(".game-details-multi");
const playerInput = document.getElementById("player");
const player1Input = document.getElementById("player-1");
const player2Input = document.getElementById("player-2");

const game = document.getElementById("game");
const details = document.getElementById("user-details");
const spanSymbol = document.getElementById("span-symbol");
const back = document.getElementById("back");

const radioX = document.getElementById("X-mp");
const radio0 = document.getElementById("0-mp");
const radioXsp = document.getElementById("X-sp");
const radio0sp = document.getElementById("0-sp");
const radioButtonsMP = document.querySelectorAll(".input-mp");
const radioButtonsSP = document.querySelectorAll(".input-sp");

const tiles = Array.from(document.querySelectorAll(".tile"));
const playerDisplay = document.querySelector(".display-player");
const resetButton = document.querySelector("#reset");
const announcer = document.querySelector(".announcer");
