window.addEventListener("DOMContentLoaded", () => {
  let player1Name, player2Name, player1Symbol, playerXName, player0Name;
  let board = ["", "", "", "", "", "", "", "", ""]; // initializare tabla de joc
  let currentPlayer = "X"; // primul jucator este X
  let currentName = playerXName; // numele primului jucator este cel al jucatorului care alege X
  let isGameActive = true;

  const PLAYERX_WON = "PLAYERX_WON";
  const PLAYERO_WON = "PLAYERO_WON";
  const TIE = "TIE";

  // situatiile in care jocul este castigat
  const winningConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  // verificare castigator
  function handleResultValidation() {
    let roundWon = false;
    // parcurgem cele 8 conditii care determina invingatorul si verificam daca avem acelasi caracter
    for (let i = 0; i <= 7; i++) {
      const winCondition = winningConditions[i];
      const firstSymbol = board[winCondition[0]];
      const secondSymbol = board[winCondition[1]];
      const thirdSymbol = board[winCondition[2]];
      // daca unul din caractere lipseste, nu mai verificam daca caracterul este acelasi
      if (firstSymbol === "" || secondSymbol === "" || thirdSymbol === "") {
        continue;
      }
      // verificam daca avem acelasi caracter
      if (firstSymbol === secondSymbol && secondSymbol === thirdSymbol) {
        roundWon = true;
        break;
      }
    }

    if (roundWon) {
      announce(currentPlayer === "X" ? PLAYERX_WON : PLAYERO_WON);
      isGameActive = false;
      return;
    }
    // daca nu mai sunt casute disponibile si nimeni nu a castigat inca, avem egaitate
    if (!board.includes("")) announce(TIE);
  }

  // mesaje sfarsit joc
  const announce = (type) => {
    switch (type) {
      case PLAYERX_WON:
        announcer.innerHTML = `Player <span class="playerX">${playerXName} (X)</span> Won`;
        break;
      case PLAYERO_WON:
        announcer.innerHTML = `Player <span class="playerO">${player0Name} (O)</span> Won`;
        break;
      case TIE:
        announcer.innerText = "Tie";
    }
    announcer.classList.remove("hide");
  };

  // verificam ca locatia unde dam click sa fie disponibila
  const isValidAction = (tile) => {
    if (tile.innerText === "X" || tile.innerText === "O") {
      return false;
    }
    return true;
  };

  // updatam array-ul cu mutarea facuta
  const updateBoard = (index) => {
    board[index] = currentPlayer;
  };

  // schimbare jucator
  const changePlayer = () => {
    if (isGameActive) {
      playerDisplay.classList.remove(`player${currentPlayer}`);
      currentPlayer = currentPlayer === "X" ? "O" : "X";
      currentName = currentName === playerXName ? player0Name : playerXName;
      playerDisplay.innerText = `${currentName} (${currentPlayer})`;
      playerDisplay.classList.add(`player${currentPlayer}`);
    }
  };
  // adaugarea simbolului pe casuta pe care am dat click
  const userAction = (tile, index) => {
    if (isValidAction(tile) && isGameActive) {
      tile.innerText = currentPlayer;
      tile.classList.add(`player${currentPlayer}`);
      updateBoard(index);
      handleResultValidation();
      changePlayer();
    }
    bot();
  };

  function bot() {
    // daca este randul bot-ului
    if (isGameActive && currentName === "Computer") {
      let array = [];
      // verificam daca mai sunt mutari disponibile
      for (let i = 0; i < board.length; i++) {
        if (board[i] === "") {
          // salvam indexul mutarilor disponibile
          array.push(i);
        }
      }
      // alegem random un index care va fi mutarea bot-ului
      let randomBox = array[Math.floor(Math.random() * array.length)];
      // daca avem mutari disponibile
      if (array.length > 0) {
        // adaugam mutarea pe tabela de joc
        tiles[randomBox].innerText = currentPlayer;
        tiles[randomBox].classList.add(`player${currentPlayer}`);
        updateBoard(randomBox);
        handleResultValidation();
        changePlayer();
      }
    }
  }
  // afisare jucator curent
  const displayName = () => {
    playerDisplay.innerText = `${currentName} (${currentPlayer})`;
    playerDisplay.classList.add(`player${currentPlayer}`);
  };

  // initializare/resetare joc
  const init = () => {
    board = ["", "", "", "", "", "", "", "", ""];
    isGameActive = true;
    announcer.classList.add("hide");
    if (currentPlayer === "O") {
      changePlayer();
    }

    tiles.forEach((tile) => {
      tile.innerText = "";
      tile.classList.remove("playerX");
      tile.classList.remove("playerO");
    });
    bot();
  };
  // la click pe casuta, apelam functia de desenare
  tiles.forEach((tile, index) => {
    tile.addEventListener("click", () => userAction(tile, index));
  });
  // la apasarea butonului de reset, reinitializam jocul
  resetButton.addEventListener("click", init);

  const clearInputs = () => {
    const inputElements = document.querySelectorAll("input");
    inputElements.forEach((input) => (input.value = ""));
  };

  // verificam ce simbol alege jucatorul
  const checkRadio = (radioBtnX, radioBtn0) => {
    if (radioBtnX.checked > 0) {
      player1Symbol = "X";
      spanSymbol.textContent = "O";
      spanSymbol.className = "playerO";
    } else if (radioBtn0.checked > 0) {
      player1Symbol = "O";
      spanSymbol.textContent = "X";
      spanSymbol.className = "playerX";
    }
  };

  const hideDetails = () => {
    details.classList.add("hide");
    game.classList.remove("hide");
    gameDetailsMulti.classList.add("hide");
    gameDetailsSingle.classList.add("hide");
  };
  // verificam simbolul selectat la fiecare click pe radio button
  radioButtonsMP.forEach((button) =>
    button.addEventListener("click", function () {
      checkRadio(radioX, radio0);
    })
  );

  radioButtonsSP.forEach((button) =>
    button.addEventListener("click", function () {
      checkRadio(radioXsp, radio0sp);
    })
  );
  // afisare detalii joc in functie de modul de joc ales
  gameButtons.forEach((btn) =>
    btn.addEventListener("click", function (e) {
      if (e.target.id === "btn-multi") {
        gameDetailsMulti.classList.toggle("hide");
        gameDetailsSingle.classList.add("hide");
        checkRadio(radioX, radio0);
      } else if (e.target.id === "btn-single") {
        gameDetailsSingle.classList.toggle("hide");
        gameDetailsMulti.classList.add("hide");
        checkRadio(radioXsp, radio0sp);
      }
    })
  );
  // initializare valori in functie de modul de joc si valorile introduse, la apasarea butonului start
  startButtons.forEach((btn) =>
    btn.addEventListener("click", function (e) {
      // e.preventDefault();
      if (e.target.id === "start-mp") {
        gameMode = 1;
        checkRadio(radioX, radio0);
        player1Name = player1Input.value;
        player2Name = player2Input.value;
      } else if (e.target.id === "start-sp") {
        gameMode = 0;
        checkRadio(radioXsp, radio0sp);
        player1Name = playerInput.value;
        player2Name = "Computer";
      }
      playerXName = player1Symbol === "X" ? player1Name : player2Name;
      player0Name = player1Symbol === "X" ? player2Name : player1Name;
      currentName = playerXName;
      clearInputs();
      hideDetails();
      init();
      displayName();
    })
  );

  back.addEventListener("click", function () {
    location.reload();
  });
});
