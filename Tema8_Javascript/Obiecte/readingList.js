const booksArray = [
{title: 'Atomic Habits',
    author: 'James Clear',
    alreadyRead: false
   },
   {title: 'Morometii',
   author: 'Marin Preda',
   alreadyRead: true
   }
];

for(let i=0; i<booksArray.length;i++){
    // 1
    const about = `${booksArray[i].title} by ${booksArray[i].author}`
    console.log(about);
    // 2
    if(booksArray[i].alreadyRead === true){
        console.log(`You already read ${about}`);
    } else{
        console.log(`You still need to read ${about}`);
    }

}