let cardCopy = "";
let errorMsg = "";

function validateCreditCard(cardNumber) {
  cardCopy = ""; // resetare valoare pentru cand avem mai multe apelari ale functiei
  errorMsg = ""; // resetare valoare
  removeChar(cardNumber);
  if (
    checkDigits(cardCopy) &&
    checkMultipleDigits(cardCopy) &&
    checkEven(cardCopy) &&
    checkSum(cardCopy)
  ) {
    return {
      valid: true,
      number: cardNumber,
    };
  } else {
    return {
      valid: false,
      number: cardNumber,
      errorMessage: errorMsg,
    };
  }
}

function removeChar(cardNumber) {
  // Cream o noua variabila in care adaugam doar cifrele, fara "-"
  if (typeof cardNumber !== "string") {
    cardNumber = cardNumber.toString();
  }
  console.log(`card num: ${cardNumber}`);
  for (let i = 0; i < cardNumber.length; i++) {
    if (cardNumber[i] !== "-") {
      cardCopy += cardNumber[i];
    }
  }
  cardCopy = Number(cardCopy);
}

function checkDigits(cardNumber) {
  // Verificam daca avem un numar (poate fi NaN dupa conversie) si daca are 16 caractere
  if (typeof cardCopy === "number" && cardCopy.toString().length === 16)
    return true;
  else {
    errorMsg += "error: wrong_length";
    return false;
  }
}

function checkMultipleDigits(cardNumber) {
  // Verificam daca toate cifrele numarului sunt la fel. Daca nu sunt la fel, conditia este indeplinita
  const first = cardNumber % 10;
  while (cardNumber) {
    if (cardNumber % 10 !== first) {
      return true;
    } else {
      cardNumber = Math.floor(cardNumber / 10);
    }
  }
  errorMsg += "error: same_digit";
  return false;
}

function checkEven(cardNumber) {
  // Verificare paritate
  console.log(cardNumber);
  if (cardNumber % 2 === 0) {
    return true;
  } else {
    errorMsg += "error: not_even";
    return false;
  }
}

function checkSum(cardNumber) {
  // Calcularea sumei numarului
  let sum = 0;
  while (cardNumber) {
    sum += cardNumber % 10;
    cardNumber = Math.floor(cardNumber / 10);
  }
  if (sum > 16) {
    return true;
  } else {
    errorMsg += "error: sum_under16";
    return false;
  }
}

console.log(validateCreditCard(1234567899999998));
console.log(validateCreditCard("1234567899999996"));
console.log(validateCreditCard(1117111111111111));
console.log(validateCreditCard("9999-7777-8888-0000"));
console.log(validateCreditCard("1111-7777-8888-0001"));
