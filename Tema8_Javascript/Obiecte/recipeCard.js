const recipeCard = {
    title: 'Mole',
    serves: 2,
    ingredients: ['cinnamon', 'cumin', 'cocoa']
};

console.log(recipeCard.title);
console.log(`${Object.keys(recipeCard)[1]}: ${recipeCard.serves}`);
console.log(`${Object.keys(recipeCard)[2]}:`);
for (let i = 0; i < recipeCard.ingredients.length; i++) {
    console.log(recipeCard.ingredients[i]);
}