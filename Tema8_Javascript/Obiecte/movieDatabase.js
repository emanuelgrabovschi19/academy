const movie = {
    title: 'Se7en',
    duration: 120,
    stars: ['Brad Pitt', 'Morgan Freeman', 'Kevin Spacey']
}

function movieDetails(movie) {
    console.log(`${movie.title} lasts for ${movie.duration}. Stars: ${movie.stars.join(', ')}`);  
}

movieDetails(movie);