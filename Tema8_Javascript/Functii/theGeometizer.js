console.log("---EXERCISE 4---");
function calcCircumference(radius){
    console.log(`The circumference is ${2*Math.PI*radius}`);
}
function calcArea(radius){
    console.log(`The area is ${Math.PI * (Math.pow(radius,2))}`);
}

calcCircumference(3);
calcArea(3);