console.log("---EXERCISE 3---");
function calculateSupply(age,amountPerDay){
    const maxAge = 80;
    let daysLeft = (maxAge - age) * 365;
    let supply = Math.round(daysLeft * amountPerDay);
    console.log(`You will need ${supply} to last you until the ripe old age of ${maxAge}`);
}

calculateSupply(25,2.3);
calculateSupply(33,1.7);
calculateSupply(29,4);