console.log("---EXERCISE 1---");
function tellFortune(childrenNumber, partnerName, geoLocation, jobTitle){
    console.log(`You will be a ${jobTitle} in ${geoLocation}, and married to ${partnerName} with ${childrenNumber} kids`);
}

tellFortune(2,"Alexandra","San Francisco","web developer");
tellFortune(4,"Raluca","Paris","personal trainer");
tellFortune(3,"Denisa","London","businessman");