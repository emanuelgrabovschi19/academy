console.log("---EXERCISE 5---");
const celsiusTemperature = 30;
const fahrenheitTemperature = 90;

function celsiusToFahrenheit(celsiusTemp){
    console.log(`${celsiusTemp}°C is ${(celsiusTemp * 1.8) + 32}°F`)
}

function fahrenheitToCelsius(fahrenheitTemp){
    console.log(`${fahrenheitTemp}°F  is ${(fahrenheitTemp -32) * 0.5556}°C.`);
}

celsiusToFahrenheit(celsiusTemperature);
fahrenheitToCelsius(fahrenheitTemperature);