console.log("---EXERCISE 2---");
function calculateDogAge(age,rate=7){
    console.log(`Your doggie is ${age*rate} years old in dog years!`);
}

calculateDogAge(5);
calculateDogAge(1);
calculateDogAge(7);