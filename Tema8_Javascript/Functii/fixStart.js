console.log("---EXERCISE 9---");
function fixStart(str) {
    let firstChar = str.charAt(0);
    return firstChar + str.slice(1).replace(new RegExp(firstChar, 'g'), '*');
  }

//   VARIANTA 2
//   function fixStart(str){
//     let strArray = str.split("");
//     let firstChar = strArray[0];
//     for(let i=1;i<strArray.length;i++){
//         if(strArray[i] === firstChar){
//             strArray[i] = '*';
//         }
//     }
//     return strArray.join('');
//   }

console.log(fixStart('babble'));