console.log("---EXERCISE 7---");
function drEvil(amount){
    const extraText = amount === 1000000 ? '(pinky)' : '';
    console.log(`DrEvil(${amount}): ${amount} dollars ${extraText}`);
}

drEvil(10);
drEvil(1000000);