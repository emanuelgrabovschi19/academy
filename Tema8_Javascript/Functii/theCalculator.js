console.log("---EXERCISE 6---");
function squareNumber(num){
    const squaredNum = Math.pow(num,2);
    console.log(`The result of squaring the number ${num} is ${squaredNum}`);
    return squaredNum;
}

function halfNumber(num){
    const halfNum = num / 2;
    console.log(`Half of ${num} is ${halfNum}`);
    return halfNum;
}

function percentOf(num1,num2){
    const percentOf = (num1/num2) * 100;
    console.log(`${num1} is ${percentOf}% of ${num2}`);
    return percentOf;
}

function areaOfCircle(radius){
    const circleArea = (Math.PI * (Math.pow(radius,2))).toFixed(2);
    console.log(`The area of a circle with radius ${radius} is ${circleArea}`);
    return circleArea;
}

function operation(num){
    const halfedNum = halfNumber(num);
    const squared = squareNumber(halfedNum);
    const circleArea = areaOfCircle(squared);
    percentOf(circleArea,squared);
}


squareNumber(3);
halfNumber(7);
percentOf(1,10);
areaOfCircle(2);
operation(7);
