console.log("---EXERCISE 10---");

function verbing(str) {
    if (str.length < 3) return str;
    if (str.slice(-3) == 'ing') { // verifica daca ultimele 3 caractere sunt 'ing'
      return str + 'ly';
    } else {
      return str + 'ing';
    }
  }

  console.log(verbing('swim'));
  console.log(verbing('swimming'));
  console.log(verbing('go'));
