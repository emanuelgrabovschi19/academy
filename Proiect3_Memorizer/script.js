let locked = false;
let clicks = 0,
  points = 0,
  // For multi-player keep the stats in arrays
  clicksMP = [0, 0],
  pointsMP = [0, 0];
let singleMode, multiMode;
let currentPlayer = 0;
let usedArray;

// 3x4 Array
const cardArray1 = [
  { name: "coffee", img: "./assets/images/coffee.jpg" },
  { name: "coffee", img: "./assets/images/coffee.jpg" },
  { name: "apple", img: "./assets/images/apple.jpg" },
  { name: "apple", img: "./assets/images/apple.jpg" },
  { name: "car", img: "./assets/images/car.jpg" },
  { name: "car", img: "./assets/images/car.jpg" },
  { name: "netflix", img: "./assets/images/netflix.jpg" },
  { name: "netflix", img: "./assets/images/netflix.jpg" },
  { name: "autumn", img: "./assets/images/autumn.jpg" },
  { name: "autumn", img: "./assets/images/autumn.jpg" },
  { name: "lemon", img: "./assets/images/lemon.jpg" },
  { name: "lemon", img: "./assets/images/lemon.jpg" },
];
// 4x4 Array
const cardArray2 = [
  ...cardArray1,
  { name: "letter", img: "./assets/images/letter.jpg" },
  { name: "letter", img: "./assets/images/letter.jpg" },
  { name: "headphones", img: "./assets/images/headphones.jpg" },
  { name: "headphones", img: "./assets/images/headphones.jpg" },
];
// 4x5 Array
const cardArray3 = [
  ...cardArray2,
  { name: "campfire", img: "./assets/images/campfire.jpg" },
  { name: "campfire", img: "./assets/images/campfire.jpg" },
  { name: "stars", img: "./assets/images/stars.jpg" },
  { name: "stars", img: "./assets/images/stars.jpg" },
];
// 4x6 Array
const cardArray4 = [
  ...cardArray3,
  { name: "infinity", img: "./assets/images/infinity.jpg" },
  { name: "infinity", img: "./assets/images/infinity.jpg" },
  { name: "colors", img: "./assets/images/colors.jpg" },
  { name: "colors", img: "./assets/images/colors.jpg" },
];

// RANDOMIZE ELEMENTS
const randomize = (inputArr) => {
  inputArr.sort(() => Math.random() - 0.5);
  return inputArr;
};

// GENERATE CARDS
const cardGenerator = (inputArr) => {
  const cardData = randomize(inputArr); // We do all the stepts on a newly randomized array
  // Generate HTML
  inputArr.forEach((element, index) => {
    // Create the card which will contain 2 sides: face and back
    const card = document.createElement("div");
    const face = document.createElement("img");
    const back = document.createElement("img");
    card.classList = "card";
    face.classList = "face";
    back.classList = "back";
    // Attach images to the cards
    face.src = element.img;
    back.src = "./assets/images/question.jpg";
    card.setAttribute("name", element.name);
    // Attach cards to the section
    gameSection.appendChild(card);
    card.appendChild(face);
    card.appendChild(back);

    card.addEventListener("click", (e) => {
      if (locked) return; // If the images are flipping, we can't click on other cards
      const flippedCards = document.querySelectorAll(".flipped");
      if (e.target === flippedCards[0]) return; // We can't click twice on the same card (to unflip it)
      card.classList.toggle("rotateCard");
      // On each click on a card, we increment the number of moves
      if (singleMode) {
        clicks++;
        displayValue(movesElem, clicks);
      }
      if (multiMode) {
        incrementClicks();
      }
      checkCards(e); // Check if the cards match
    });
  });
};
// CHECK IF THE CARDS MATCH
const checkCards = (e) => {
  const clickedCard = e.target;
  clickedCard.classList.add("flipped");
  let flippedCards = document.querySelectorAll(".flipped");
  // If there are 2 card flipped
  if (flippedCards.length === 2) {
    // If they have the same name (same card)
    if (
      flippedCards[0].getAttribute("name") ===
      flippedCards[1].getAttribute("name")
    ) {
      // The player wins a point
      if (singleMode) {
        points++;
        displayValue(pointsElem, points);
      } else if (multiMode) {
        incrementScore();
      }
      // Check if the winning condition is met
      checkWin(usedArray);
      // Remove the possibility of unflipping the flipped cards
      flippedCards.forEach((card) => {
        card.classList.remove("flipped");
        card.style.pointerEvents = "none";
      });
    } else {
      locked = true; // Can't click on other cards while unflipping
      // For multiplayer - change the current player
      if (multiMode) {
        setTimeout(() => {
          swapPlayers();
        }, 1000);
      }
      // Unflip the cards
      flippedCards.forEach((card) => {
        card.classList.remove("flipped");
        setTimeout(() => {
          card.classList.remove("rotateCard");
          locked = false;
        }, 1000);
      });
    }
  }
};
// Display an item on an HTML element
const displayValue = (elem, display) => {
  elem.textContent = display;
};
// Based on how many rows and column the user wants, choose the appropiate array with images and grid style
const checkLevel = () => {
  if (radioEasy.checked) {
    usedArray = cardArray1;
    gameSection.classList.add("grid-1");
  } else if (radioMed.checked) {
    usedArray = cardArray2;
    gameSection.classList.add("grid-2");
  } else if (radioHard.checked) {
    usedArray = cardArray3;
    gameSection.classList.add("grid-3");
  } else if (radioVeryHard.checked) {
    usedArray = cardArray4;
    gameSection.classList.add("grid-4");
  }
  cardGenerator(usedArray);
};
// Check if the user chose Single or Multi
const checkMode = () => {
  if (radioSingle.checked) {
    singleMode = true;
  } else if (radioMulti.checked) {
    multiMode = true;
  }
};

const checkWin = (inputArr) => {
  // The winner is chosen after we have no more cards to flip (all of them are flipped)
  if (document.querySelectorAll(".card:not(.rotateCard)").length) return;
  let msg = "";
  // Generate winning message
  if (singleMode) {
    msg = `You won with ${points} points and ${clicks} clicks!`;
  } else {
    // Player1 wins
    if (pointsMP[0] > pointsMP[1]) {
      msg = `Player1 won the game with ${pointsMP[0]} points and ${clicksMP[0]} clicks!`;
      // Player2 wins
    } else if (pointsMP[1] > pointsMP[0]) {
      msg = `Player2 won the game with ${pointsMP[1]} points and ${clicksMP[1]} clicks!`;
    } else {
      // TIE
      if (clicksMP[0] === clicksMP[1]) {
        msg = `It's a tie!`;
        // Same score but less moves wins
      } else if (clicksMP[0] < clicksMP[1]) {
        msg = `Player1 wins at number of moves (${clicksMP[0]} < ${clicksMP[1]}) `;
      } else if (clicksMP[1] < clicksMP[0]) {
        msg = `Player2 wins at number of moves (${clicksMP[1]} < ${clicksMP[0]}) `;
      }
    }
  }
  displayValue(messageElem, msg);
};

// Change current player
const swapPlayers = () => {
  currentPlayer = currentPlayer === 0 ? 1 : 0;
  playerStats0.classList.toggle("player-active");
  playerStats1.classList.toggle("player-active");
};

// Adds clicks for the current player
const incrementClicks = () => {
  clicksMP[currentPlayer]++;
  document.querySelector(`.moves-${currentPlayer}`).textContent =
    clicksMP[currentPlayer];
};

// Adds points for the current player
const incrementScore = () => {
  pointsMP[currentPlayer]++;
  document.querySelector(`.points-${currentPlayer}`).textContent =
    pointsMP[currentPlayer];
};
// RESET
const init = (inputArr) => {
  clicks = 0;
  points = 0;
  clicksMP = [0, 0];
  pointsMP = [0, 0];
  currentPlayer = 0;
  if (!playerStats0.classList.contains("player-active")) {
    playerStats0.classList.add("player-active");
  }
  playerStats1.classList.remove("player-active");
  numberElems.forEach((el) => {
    displayValue(el, "0");
  });
  displayValue(messageElem, "");
  let cardData = randomize(inputArr);
  let faces = document.querySelectorAll(".face");
  let cards = document.querySelectorAll(".card");
  // while fliping the cards for reset, can't click on anything
  gameSection.style.pointerEvents = "none";
  cardData.forEach((element, index) => {
    cards[index].classList.remove("rotateCard");
    setTimeout(() => {
      faces[index].src = element.img;
      cards[index].setAttribute("name", element.name);
      cards[index].style.pointerEvents = "all";
      gameSection.style.pointerEvents = "all";
    }, 1500);
  });
};

// EVENT LISTENERS - BUTTONS
btnStart.addEventListener("click", () => {
  checkMode();
  checkLevel();

  setTimeout(() => {
    gameDetails.classList.add("hide");
    gameContainer.classList.remove("hide");
    if (singleMode) {
      statsSingle.classList.remove("hide");
    } else if (multiMode) {
      statsMulti.classList.remove("hide");
    }
  }, 100);
});

btnReset.addEventListener("click", () => {
  init(usedArray);
  // gameSection.style.pointerEvents = "none";
  // setTimeout(() => {
  //   gameSection.style.pointerEvents = "all";
  // }, 1500);
});

btnMenu.addEventListener("click", () => {
  location.reload();
});
