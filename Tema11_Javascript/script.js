let json = '{"name":"John", "age": 30}';
let json2 = '{"name":"John"}';
let json3 = '{"gender":"male"}';
let json4 = '';

class ValidationError extends Error {
    constructor(message) {
      super(message);
      this.name = "ValidationError";
    }
   checkType(obj){
    if(typeof obj.name !== 'string'){
        throw new ValidationError("The name is not a string!");
    }
    if(typeof obj.age !== 'number'){
        throw new ValidationError("The age is not a number");
    }
   }
  }
  
  function displayUserProperties(json) {
    let user = JSON.parse(json);

    if(!user.name && !user.age){
        throw new ValidationError("Proeprties not here... ");
    }
    if (!user.name) {
        throw new ValidationError("Name proeprty not here... ");
      }
    if (!user.age) {
      throw new ValidationError("Age property not here...");
    }
    return user;
  }
  
  try {
    let user = displayUserProperties(json2);
    let validate = new ValidationError("");
    validate.checkType(json2);
  } catch (err) {
      console.log(`JSON missing property: ${err.message}`); 
  }
