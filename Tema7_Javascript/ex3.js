let currentAge = 25;
const maximumAge = 80;
const estAmount = 2;

let yearsLeft = maximumAge-currentAge;
let noDaysLeft = yearsLeft*365;

let noPacks = estAmount*noDaysLeft;

alert(`You will need ${noPacks} to last you until the ripe old age of ${maximumAge}`);