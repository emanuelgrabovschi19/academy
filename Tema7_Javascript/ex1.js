const numberOfChildren = 2;
const partnerName = "Alexandra";
const geographicLocation = "California";
const jobTitle = "web developer";

alert(`You will be a ${jobTitle} in ${geographicLocation}, and married to ${partnerName} with ${numberOfChildren} kids`);