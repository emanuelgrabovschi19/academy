const spaceShuttleName = "Determination";
let shuttleSpeedMph = 17500;
const distanceToMarsKm = 225000000;
const distanceToMoonKm = 384400;
const milesToKm = 0.621;

const milesToMars = distanceToMarsKm * milesToKm;
const hoursToMars = milesToMars / shuttleSpeedMph;
const daysToMars = hoursToMars / 24;

// 6.3
alert(`${spaceShuttleName} will take ${daysToMars} days to reach Mars`);

// 6.4
const milesToMoon = distanceToMoonKm * milesToKm;
const hoursToMoon = milesToMoon / shuttleSpeedMph;
const daysToMoon = hoursToMoon / 24;

alert(`${spaceShuttleName} will take ${daysToMoon} days to reach Moon`);
